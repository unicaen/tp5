# TP5 #

Ce TP se place dans la continuité des précédents :

* Par rapport au TP3, nous allons étendre l'interface « Directory » pour lui donner plus de fonctionnalités.

* Par rapport au TP4, nous allons étendre l'interface utilisateur de façon à tenir compte de ces nouvelles fonctionnalités, et lui donner des capacités d'édition des données.


Voir l'énoncé complet sur le [Moodle](http://foad2.unicaen.fr/moodle/course/view.php?id=24560).