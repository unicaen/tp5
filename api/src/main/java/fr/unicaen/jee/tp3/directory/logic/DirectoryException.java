package fr.unicaen.jee.tp3.directory.logic;

/**
 * @author Frédérik Bilhaut
 */
public class DirectoryException extends Exception {
	
	public DirectoryException(Throwable t) {
		super(t);
	}
	
}
