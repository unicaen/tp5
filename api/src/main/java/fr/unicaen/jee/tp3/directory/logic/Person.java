package fr.unicaen.jee.tp3.directory.logic;

/**
 * Objet métier décrivant une personne
 * @author Frédérik Bilhaut
 */
public class Person extends DirectoryObject {
	
	private String lastName;
	private String firstName;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
		
	@Override
	public String toString() {
		return getId() + ", " + this.firstName + ", " + this.lastName;
	}
	
	
}
