package fr.unicaen.jee.tp3.directory.logic;

/**
 * Classe mère de tous les objets gérés par le Directory.
 * @author Frédérik Bilhaut
 */
public class DirectoryObject {
	
	private long id;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
}
