package fr.unicaen.jee.tp3.directory.logic;

import java.util.*;


/**
 * Contrat abstrait d'un Directory (annuaire) version 2.0
 */
public interface Directory {
		
	/** 
	 * Permet de récupérer un ensemble de personnes dans un ordre arbitraire.
	 * Le sous-ensemble des personnes à récupérer et donné par les paramètres offset et limit.
	 * L'ordre est arbitraire mais il est garanti qu'il sera le même pour tous les appels.
	 * @param offset Index de la première personne à récupérer 
	 * @param limit Nombre maximum de personnes à récupérer
	 * @return Sous-ensemble de personnes
	 * @throws DirectoryException 
	 */
	public Collection<Person> getPersons(long offset, long limit) throws DirectoryException;
	
	/**
	 * Retourne une personne donnée par son identifiant (id)
	 * @param id Identifiant de la personne
	 * @return La personne trouvée, ou null si l'identifiant n'existe pas
	 * @throws DirectoryException 
	 */
	public Person getPerson(long id) throws DirectoryException;
	
	
	/**
	 * Met à jour les données d'une personne.
	 * @param person La personne à modifier. Elle doit nécessairement exister dans la base (la clef étant l'identifiant).
	 * @return true si une et une seule ligne est impactée
	 * @throws DirectoryException 
	 */
	public boolean updatePerson(Person person) throws DirectoryException;
	
	
	/**
	 * Retourne les personnes qui sont référencées comme relation de la personne donnée par son identifiant
	 * @param personId Identifiant de la personne dont on veut rechercher les relations
	 * @return Ensemble des relations
	 * @throws DirectoryException 
	 */
	public Collection<Person> getPersonRelations(long personId) throws DirectoryException;
	
	
	/**
	 * Ajoute une relation entre deux personnes
	 * @param person
	 * @param otherPerson
	 * @param label
	 * @return true si une et une seule ligne est impactée
	 * @throws DirectoryException 
	 */
	public boolean addRelation(Person person, Person otherPerson, String label) throws DirectoryException;
		
	
	/**
	 * Retourne les personnes dont le nom ou le prénon ressemble au pattern donné
	 * @param pattern Pattern du type abc%def%
	 * @return Ensemble de personnes
	 * @throws DirectoryException 
	 */
	public Collection<Person> searchPersons(String pattern) throws DirectoryException;
	
	
	/**
	 * Libère les ressources occupées par cette instance d'annuaire
	 * @throws fr.unicaen.jee.tp3.directory.logic.DirectoryException
	 */
	public void dispose() throws DirectoryException;
	
}
