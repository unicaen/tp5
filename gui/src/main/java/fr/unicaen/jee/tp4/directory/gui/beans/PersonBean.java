package fr.unicaen.jee.tp4.directory.gui.beans;

import java.util.*;
import javax.faces.bean.*;
import fr.unicaen.jee.tp3.directory.logic.*;


/**
 * @author Frédérik Bilhaut
 */
@ManagedBean(name="personBean")
@ViewScoped
public class PersonBean {
		
    private Long id = null;
	private Person person = null;
	private List<Person> relations = null;
	private String searchPattern = "%";
	private List<Person> searchResults = new LinkedList<>();
	
	@ManagedProperty(value="#{directoryBean}")
	private DirectoryBean directoryBean;		
	
	// Pour le bean injecté
	public DirectoryBean getDirectoryBean() {
		return directoryBean;
	}
	
	// Pour le bean injecté
	public void setDirectoryBean(DirectoryBean directoryBean) {
		this.directoryBean = directoryBean;
	}
	
	// Pour l'initialisation du bean lors de la création de la vue : charge la personne et ses relations
	public void init() throws DirectoryException {
		if(this.id != null) {
			this.person = this.directoryBean.getDirectory().getPerson(this.id);
			this.relations = new LinkedList<>(this.directoryBean.getDirectory().getPersonRelations(this.id));
		}
	}
	
	// Mise à jour des données dans le backend
	public void update() throws DirectoryException {
		if(this.person != null)
			this.directoryBean.getDirectory().updatePerson(this.person);
	}

	// Pour la propriété "id"
	public Long getId() {
		return this.id;
	}

	// Pour la propriété "id"
	public void setId(Long id) {
		this.id = id;		
	}

	// Permet de récupérer la personne chargée par init()
	public Person getValue() throws DirectoryException {		
		return this.person;
	}
	
	
	// Permet de récupérer les relations de la personne chargée par init()
	public List<Person> getRelations() throws DirectoryException {
		return this.relations;			
	}

	// Pour la fonction de recherche
	public String getSearchPattern() {
		return searchPattern;
	}

	// Pour la fonction de recherche
	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}	
	
	// Pour la fonction de recherche
	public void search() throws DirectoryException {
		this.searchResults = new LinkedList<>(this.directoryBean.getDirectory().searchPersons(this.searchPattern));
	}

	// Pour la fonction de recherche
	public List<Person> getSearchResults() {
		return this.searchResults;
	}
	
	// Pour l'ajout d'une relation
	public void addRelationWith(Person otherPerson) throws DirectoryException {
		if(this.person != null) {
			this.directoryBean.getDirectory().addRelation(this.person, otherPerson, "Default");
			this.relations = new LinkedList<>(this.directoryBean.getDirectory().getPersonRelations(this.id));
		}
	}
	
	
	
}
