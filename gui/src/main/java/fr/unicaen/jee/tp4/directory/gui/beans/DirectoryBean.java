package fr.unicaen.jee.tp4.directory.gui.beans;


import javax.faces.bean.*;

import fr.unicaen.jee.tp3.directory.logic.*;
import fr.unicaen.jee.tp3.directory.logic.db.*;


/**
 * @author Frédérik Bilhaut
 */
@ManagedBean(name="directoryBean")
@ApplicationScoped
public class DirectoryBean {
	
	private final Directory directory;
	
	
	public DirectoryBean() throws DirectoryException {
		this.directory = new DatabaseDirectory("jdbc:derby://localhost:1527/TP5");
	}
	
	
	public Directory getDirectory() {
		return this.directory;
	}
	
	
}
