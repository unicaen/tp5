/*** CONNECT 'jdbc:derby://localhost:1527/TP5;create=true';  */


/*** SAMPLE PERSONS */

INSERT INTO Person VALUES (1, 'Durand', 'Henri');
INSERT INTO Person VALUES (2, 'Durand', 'Henriette');
INSERT INTO Person VALUES (3, 'Dupuis', 'Paul');
INSERT INTO Person VALUES (4, 'Marie', 'Albert');
INSERT INTO Person VALUES (5, 'Legrand', 'Jacques');
INSERT INTO Person VALUES (6, 'Lepetit', 'Ophélie');
INSERT INTO Person VALUES (7, 'Leblanc', 'Clémence');
INSERT INTO Person VALUES (8, 'Leblanc', 'Robert');
INSERT INTO Person VALUES (9, 'Martin', 'Gertrude');
INSERT INTO Person VALUES (10, 'Bernard', 'Théophile');
INSERT INTO Person VALUES (11, 'Lambert', 'Pauline');
INSERT INTO Person VALUES (12, 'Perrin', 'Alberta');

/*** SAMPLE RELATIONS */

INSERT INTO Relation VALUES (1, 2, 'Conjoint');
INSERT INTO Relation VALUES (2, 1, 'Conjoint');
INSERT INTO Relation VALUES (3, 1, 'Chef');
INSERT INTO Relation VALUES (1, 2, 'Subordonné');
INSERT INTO Relation VALUES (4, 1, 'Ami');
INSERT INTO Relation VALUES (1, 4, 'Ami');
INSERT INTO Relation VALUES (7, 8, 'Conjoint');
INSERT INTO Relation VALUES (8, 7, 'Conjoint');
INSERT INTO Relation VALUES (3, 7, 'Chef');
INSERT INTO Relation VALUES (7, 3, 'Subordonné');
INSERT INTO Relation VALUES (1, 7, 'Ami');
INSERT INTO Relation VALUES (7, 1, 'Ami');
INSERT INTO Relation VALUES (2, 7, 'Ami');
INSERT INTO Relation VALUES (7, 2, 'Ami');