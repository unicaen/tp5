package fr.unicaen.jee.tp3.test;

import org.junit.*;
import fr.unicaen.jee.tp3.directory.logic.*;
import fr.unicaen.jee.tp3.directory.logic.db.*;

/**
 * @author Frédérik Bilhaut
 * 
 * REMARQUE : Ces tests ne sont pas des tests unitaires exemplaires !
 * Il faudrait ajouter des assert() etc.
 */
public class DatabaseDirectoryTest {
	
	@Test
	public void loadPersons() throws Exception {
		System.out.println("\n*** Load persons :");
		try(DatabaseDirectory directory = new DatabaseDirectory("jdbc:derby://localhost:1527/TP5")) {
			directory.getPersons(1, 2).stream().forEach(x -> System.out.println(x));
		}
	}
	
	
	@Test
	public void loadRelations() throws Exception {
		System.out.println("\n*** Load relations :");
		try(DatabaseDirectory directory = new DatabaseDirectory("jdbc:derby://localhost:1527/TP5")) {
			directory.getPersonRelations(1).stream().forEach(x -> System.out.println(x));
		}
	}
	
	
	@Test
	public void updatePerson() throws Exception {
		System.out.println("\n*** Update :");
		try(DatabaseDirectory directory = new DatabaseDirectory("jdbc:derby://localhost:1527/TP5")) {
			Person person = directory.getPerson(1);
			person.setFirstName("James");
			person.setLastName("Bond");
			System.out.println(directory.updatePerson(person));
		}
	}
	
	
	@Test
	public void searchPersons() throws Exception {
		System.out.println("\n*** Search :");
		try(DatabaseDirectory directory = new DatabaseDirectory("jdbc:derby://localhost:1527/TP5")) {
			directory.searchPersons("Mar%").stream().forEach(x -> System.out.println(x));
		}
	}
	
}
