package fr.unicaen.jee.tp3.directory.logic.db;

/**
 * @author Frédérik Bilhaut
 */
public interface Schema {
	
	static final String TABLE_PERSON = "Person";	
	static final String COL_PERSON_ID = "PersonId";
	static final String COL_PERSON_LAST_NAME = "LastName";
	static final String COL_PERSON_FIRST_NAME = "FirstName";
	
	static final String TABLE_RELATION = "Relation";	
	static final String COL_RELATION_PERSON_ID = "PersonId";
	static final String COL_RELATION_OTHER_PERSON_ID = "OtherPersonId";
	static final String COL_RELATION_TYPE = "Type";
	
}
